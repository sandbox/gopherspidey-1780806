<?php
class SeriesMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->source = new MigrateSourceCSV('/home/spidey/Sermons-written/Series.csv', array(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('message_series');

    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'Series' => array(
 			  'type' => 'varchar',
                          'length' => 255,
			  'not null' => TRUE,
                         )
        ),
        MigrateDestinationNode::getKeySchema()
      );

    // Finally we add simple field mappings 
    $this->addFieldMapping('title', 'Series');
    $this->addFieldMapping('created', 'Start Date');
  }
}
