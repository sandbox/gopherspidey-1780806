<?php
class LoginsMigration extends Migration {
  public function __construct() {
    parent::__construct(MigrateGroup::getInstance('GDLC'));

    $this->source = new MigrateSourceCSV('/home/spidey/Sermons-written/Logins.csv', array(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationUser();

    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'username' => array('type' => 'varchar',
                          'length' => 32,
                         )
        ),
        MigrateDestinationNode::getKeySchema()
      );

    // Finally we add simple field mappings 
    $this->addFieldMapping('name', 'username');
    $this->addFieldMapping('mail', 'Email');
    $this->addFieldMapping('status', 'Active');
    $this->addFieldMapping('pass')->defaultValue('1qaz@WSX');
  }
  public function prepareRow($row) {
	if ($row->Active == 'yes') {
		$row->Active = 1;
	} else {
		$row->Active = 0;
	}
  }
}


