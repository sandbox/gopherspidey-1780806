<?php
class MessagesMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->source = new MigrateSourceCSV('/home/spidey/Sermons-written/Messages.csv', array(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationNode('message');
    $this->dependencies = array('Logins', 'Series');

    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'Date' => array('type' => 'varchar',
                          'length' => 32,
                         )
        ),
        MigrateDestinationNode::getKeySchema()
      );

    // Finally we add simple field mappings 
    $this->addFieldMapping('title', 'Title');
    $this->addFieldMapping('field_date', 'Date')
         ->callbacks(array($this, 'removeUnderscore'));
    $this->addFieldMapping('field_message_keywords', 'Key Words')
         ->separator(',');
    $this->addFieldMapping('field_message_keywords:create_term')
         ->defaultValue(TRUE);
    $this->addFieldMapping('field_pastor_ref', 'Pastor')
         ->sourceMigration('Logins')
         ->separator(',');
    $this->addFieldMapping('field_message_series_ref', 'Series')
         ->sourceMigration('Series')
         ->defaultValue(NULL);
    $this->addFieldMapping('field_transcript', 'transcript');
    $this->addFieldMapping('field_audio_recording:file_class')->defaultValue('MigrateFileUri');
    $this->addFieldMapping('field_audio_recording:destination_dir', 'destdir');
    $this->addFieldMapping('field_audio_recording', 'Audio Link');
//    $this->addFieldMapping('field_audio_recording:destination_file', 'Audio Link')->callbacks('basename');
//    $this->addFieldMapping('field_audio_recording:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('is_new')->defaultValue(FALSE);
  }
  protected function emptytonull($string) {
    if (empty($string)) {
	return NULL;
    }
  }
  protected function removeUnderscore($date) {
    return str_replace('_', '-', $date);
  }
  public function prepareRow($row) {
    if (empty($row->Title)) return FALSE;
   
    $filedate = str_replace('-', '_', $row->Date);
    $file = array_shift(glob('/home/spidey/Sermons-written/transcripts/' . $filedate . '*.doc.txt'));   
    if (is_file($file)) {
       $row->transcript = file_get_contents($file);
    } else {
	unset($row->transcript);
    }
    if (empty($row->Series)) {
      unset($row->Series);
    }
    $row->destdir = 'public://message/' . substr($row->Date, 0, 4) . '/';
    
  }
  public function destdir($value) {
   return 'public://message/' . substr($sourceValues->Date, 0, 4) . '/';
  }
  public function prepare($entity, $row) {
    if (empty($row->transcript)) {
      unset($entity->field_transcript);
    }
    if (empty($row->Series)) {
      self::displayMessage(print_r($entity));
      unset($entity->field_message_series_ref);
    }
    if (is_null($entity->field_message_series_ref['und'][0]['target_id'])) {
      unset($entity->field_message_series_ref);
    }
  }
}
